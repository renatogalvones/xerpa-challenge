const addStarMutation = `
  mutation AddStarMutation($input: AddStarInput!) {
    addStar(input: $input) {
      clientMutationId,
      starrable {
        viewerHasStarred,
        stargazerCount
      }
    }
  }
`;

module.exports = addStarMutation;
