const getUserStarredRepos = `
 query getStarRepos (
  $login: String!
) {
  user (login: $login) {
    id
    bio
    location
    name
    avatarUrl
    websiteUrl
    email
    login
    followers(first:1){
      totalCount
    }
    starredRepositories(first: 4, ) {
      pageInfo {
        endCursor
        startCursor
      }
      edges {
        node {
          id
          name
          description
          stargazerCount
          viewerHasStarred
        }
      }
    }
  }
}

`;

module.exports = getUserStarredRepos;
