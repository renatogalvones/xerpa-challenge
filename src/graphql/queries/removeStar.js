const removeStarMutation = `
  mutation RemoveStarMutation($input:RemoveStarInput!) {
    removeStar(input: $input) {
      clientMutationId,
      starrable {
        viewerHasStarred,
        stargazerCount
      }
    }
  }
`;

module.exports = removeStarMutation;
