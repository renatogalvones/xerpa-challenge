const getLoggedUserInfo = `
  query {
    viewer {
      login
      name
      location
      bio
      avatarUrl
    }
  }
`;

module.exports = getLoggedUserInfo;
