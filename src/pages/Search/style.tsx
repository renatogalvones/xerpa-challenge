import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  flex-flow: column;
  max-width: 890px;
  margin: -55px auto 0;
`;

export const Spacer = styled.div`
  margin-bottom: 55px;
`;