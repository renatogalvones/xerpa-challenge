import { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router';

import { AuthContext, SearchContext, UserContext } from '../../App';

import GithubStars from '../../components/GithubStars';
import SearchBox from '../../components/SearchBox';

import {
  Wrapper,
  Spacer,
} from './style';

const Search = () => {
  const { state } = useContext(AuthContext);
  const { userDispatch } = useContext(UserContext);
  const { searchDispatch } = useContext(SearchContext);
  const [mustRedirect, setMustRedirect] = useState('');

  const { proxyUrl, accessToken } = state;

  if (!accessToken) window.location.href = '/login';

  useEffect(() => {
    const body = { endpoint: 'getloggeduserinfo' };
    const headers = { headers: { 'Authorization': `Bearer ${accessToken}` } };

    // check if user has a valid token
    axios
      .post(`${proxyUrl}/github-graphql`, body, headers)
      .then((res) => {
        const { viewer } = res.data.data;

        userDispatch({
          type: 'SET_USER',
          data: {
            ...viewer
          },
        });
      })
      .catch(error => {
        console.log(error);
        setMustRedirect('/login');
      });
  }, [])

  const handleSubmit = (username: string) => {
    if (!username || username.length < 1) return;

    const body = { endpoint: 'getuserstarredrepos', login: username };
    const headers = { headers: { 'Authorization': `Bearer ${accessToken}` } };

    axios
      .post(`${proxyUrl}/github-graphql`, body, headers)
      .then((res) => {
        const { user } = res.data.data;

        if (!user) return setMustRedirect('/notfound');

        searchDispatch({
          type: 'SET_DATA',
          data: {
             ...user,
          }
        });

        setMustRedirect('/result');
      })
      .catch(error => {
        console.log(error);
      });

  }

  return (
    <Wrapper>
      {mustRedirect && <Redirect to={mustRedirect} />}
      <Spacer>
        <GithubStars />
      </Spacer>

      <SearchBox layout="default" placeholder="github username..." handleSubmit={handleSubmit} />
    </Wrapper>
  )
}

export default Search;
