import styled, { keyframes } from 'styled-components';


const swing = keyframes`
  15%{
    transform: translateX(5px);
  }
  30%{
    transform: translateX(-5px);
  }
  50%{
    transform: translateX(3px);
  }
  65%{
    transform: translateX(-3px);
  }
  80%{
    transform: translateX(2px);
  }
  100%{
    transform: translateX(0);
  }
`;


export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  flex-flow: column;

  a:hover {
    img {
      animation: 1s ${swing} ease-out;
    }
  }

`;

