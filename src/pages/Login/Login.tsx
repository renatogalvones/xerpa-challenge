import { useContext, useEffect, useState } from 'react';
import axios from 'axios';

import { AuthContext } from '../../App';

import GithubLogo from '../../assets/images/github-mark-120px-plus.png';

import {
  Wrapper
} from './style';
import { Redirect } from 'react-router-dom';

const Login = () => {
  const { state, dispatch } = useContext(AuthContext);
  const { proxyUrl,  clientId } = state;
  const [mustRedirect, setMustRedirect] = useState('');

  useEffect(() => {
    const url = window.location.href;
    const urlSplit = url.split('?code=');
    const code = urlSplit[1];

    if (code && code.length > 0) {
      axios.get(`${proxyUrl}/gettoken?code=${code}`).then((response) => {
        const oauthResponse = 'http://fakeurl/?' + response.data;
        const params = new URL(oauthResponse).searchParams;
        const accessToken = params.get('access_token');

        if (accessToken) {
          dispatch({ type: 'SAVE_TOKEN', data: { accessToken }});
          setMustRedirect('/search');
        }
      }).catch(error => console.log(error));
    }
  }, [])

  return (
    <Wrapper>
      {mustRedirect && <Redirect to={mustRedirect} />}
      <p>
        <a href={`https://github.com/login/oauth/authorize?client_id=${clientId}&scope=public_repo,user`}>
          <img src={GithubLogo} alt="login to github" />
        </a>
      </p>
      <p>Before start, click on the logo above to authorize the app with Github.</p>
    </Wrapper>
  )
}

export default Login;
