import { useContext, useState } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router';

import { AuthContext, SearchContext, UserContext } from '../../App';

import GithubStars from '../../components/GithubStars';
import SearchBox from '../../components/SearchBox';
import UserAvatar from '../../components/UserAvatar';

import sadIcon from '../../assets/icons/sad.svg';

import {
  Wrapper,
  Header,
  Content,
  SpacerLeft,
  SpacerRight,
} from './style';

const NotFound = () => {
  const { state } = useContext(AuthContext);
  const { userState } = useContext(UserContext);
  const { searchDispatch } = useContext(SearchContext);
  const [mustRedirect, setMustRedirect] = useState('');

  const { proxyUrl, accessToken } = state;

  const handleSubmit = (username: string) => {
    if (!username || username.length < 1) return;

    const body = { endpoint: 'getuserstarredrepos', login: username };
    const headers = { headers: { 'Authorization': `Bearer ${accessToken}` } };

    axios
      .post(`${proxyUrl}/github-graphql`, body, headers)
      .then((res) => {
        const { user } = res.data.data;
        if (!user) return;

        searchDispatch({
          type: 'SET_DATA',
          data: {
            ...user,
          }
        });

        setMustRedirect('/result');
      })
      .catch(error => {
        console.log(error);
      });

  }

  return (
    <Wrapper>
      {mustRedirect && <Redirect to={mustRedirect} />}
      <Header>
        <SpacerLeft>
          <GithubStars />
        </SpacerLeft>
        <SpacerRight>
          <SearchBox layout="basic" placeholder="github username..." handleSubmit={handleSubmit} />
          <UserAvatar avatarUrl={userState.avatarUrl} />
        </SpacerRight>
      </Header>
      <Content>
        <img src={sadIcon} alt="" />
      </Content>
    </Wrapper>
  )
}

export default NotFound;
