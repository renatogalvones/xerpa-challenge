import { useContext, useState } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';

import { AuthContext, SearchContext, UserContext } from '../../App';

import GithubStars from '../../components/GithubStars';
import SearchBox from '../../components/SearchBox';
import Sidebar from '../../components/Sidebar';
import UserAvatar from '../../components/UserAvatar';
import Card from '../../components/Card';

import 'react-toastify/dist/ReactToastify.css';

import {
  Wrapper,
  Header,
  Content,
  SpacerLeft,
  SpacerRight,
  List,
} from './style';

const Login = () => {
  const { state } = useContext(AuthContext);
  const { userState } = useContext(UserContext);
  const { searchState, searchDispatch } = useContext(SearchContext);
  const [mustRedirect, setMustRedirect] = useState('');

  const { proxyUrl, accessToken } = state;

  const handleSubmit = (username: string) => {
    if (!username || username.length < 1) return;

    const body = { endpoint: 'getuserstarredrepos', login: username };
    const headers = { headers: { 'Authorization': `Bearer ${accessToken}` } };

    axios
      .post(`${proxyUrl}/github-graphql`, body, headers)
      .then((res) => {
        const { user } = res.data.data;
        if (!user) setMustRedirect('/notfound');

        searchDispatch({
          type: 'SET_DATA',
          data: {
            ...user,
          }
        });
      })
      .catch(error => {
        console.log(error);
      });

  }

  const renderRepos = () => {
    const { starredRepositories } = searchState;
    return starredRepositories.edges.map((repo: any) => {
      const { name, description, id, stargazerCount, viewerHasStarred } = repo.node;
      return <Card key={id} repoName={name} repoDescription={description} starsNumber={stargazerCount} starred={viewerHasStarred} handleClick={() => handleClick(id, viewerHasStarred)} />
    });
  }

  const handleClick = (id:string, active: boolean) => {
    const body = {
      endpoint: active ? 'removestar' : 'addstar',
      starrableId: id,
    };
    const headers = { headers: { 'Authorization': `Bearer ${accessToken}` } };

    axios
      .post(`${proxyUrl}/github-graphql`, body, headers)
      .then((res) => {
        const { errors } = res.data;

        if (errors && errors[0].type === 'FORBIDDEN') {
          return toast('Action Forbidden by repo owner.', {
            position: "bottom-left",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: false,
            progress: undefined,
          });
        }

        searchDispatch({
          type: 'UPDATE_STAR',
          data: {
            id,
            active
          },
        });

        toast(!active ? 'Repo starred with success!' : 'Repo unstarred with success!', {
          position: "bottom-left",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: false,
          progress: undefined,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }


  return (
    <Wrapper>
      {mustRedirect && <Redirect to={mustRedirect} />}
      <Header>
        <SpacerLeft>
          <GithubStars />
        </SpacerLeft>
        <SpacerRight>
          <SearchBox layout="basic" placeholder="github username..." handleSubmit={handleSubmit} />
          <UserAvatar avatarUrl={userState.avatarUrl} />
        </SpacerRight>
      </Header>
      <Content>
        <SpacerLeft>
          <Sidebar
            fullname={searchState.name}
            username={searchState.login}
            avatarUrl={searchState.avatarUrl}
            bio={searchState.bio}
            followers={searchState.followers.totalCount}
            location={searchState.location}
            email={searchState.email}
            websiteUrl={searchState.websiteUrl}
          />
        </SpacerLeft>
        <List>
          {renderRepos()}
        </List>
      </Content>
      <ToastContainer
        position="bottom-left"
        autoClose={3000}
        hideProgressBar={true}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable={false}
        pauseOnHover={false}
      />
    </Wrapper>
  )
}

export default Login;
