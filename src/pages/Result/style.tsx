import styled, { keyframes } from 'styled-components';

import { primary } from '../../theme/pallet';

export const Wrapper = styled.div`
  height: 100%;
`;

export const SpacerLeft = styled.div`
  flex-basis: 17%;
`;

export const SpacerRight = styled.div`
  flex-basis: 80%;
  display: flex;
  justify-content: space-between;
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  max-width: 1115px;
  margin: 60px auto 25px;
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
  max-width: 1115px;
  margin: 0 auto;
`;

const animation = keyframes`
  from{
    opacity: 0;
  }
  to{
    opacity: 1;
  }
`;


export const List = styled.div`
  flex-basis: 80%;
  padding: 32px;
  background-color: ${primary.alpha500};
  box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
  border-radius: 5px;

  > article {
    animation: .5s ${animation} ease-in;

    &:nth-child(2n) {
      animation: 1s ${animation} ease-in;
    }
    &:nth-child(3n) {
      animation: 1.5s ${animation} ease-in;
    }
    &:nth-child(4n) {
      animation: 2s ${animation} ease-in;
    }
  }
`;