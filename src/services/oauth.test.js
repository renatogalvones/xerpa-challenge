const request = require("supertest");
const app = require("./oauth");

describe("/gettoken method", () => {
  test("without param should return 400", async () => {
    const response = await request(app).get("/gettoken");
    expect(response.statusCode).toBe(400);
    expect(response.body).toEqual({ message: 'No code has been passed' });
  });
});