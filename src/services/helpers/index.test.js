import getQuery from './index';

import getLoggedUserInfo from '../../graphql/queries/getLoggedUserInfo';
import getUserStarredRepos from '../../graphql/queries/getUserStarredRepos';
import addStar from '../../graphql/queries/addStar';
import removeStar from '../../graphql/queries/removeStar';


test('getQuery', () => {
  expect(getQuery({})).toEqual({});
  expect(getQuery({endpoint: 'getloggeduserinfo'})).toEqual({
    query: getLoggedUserInfo,
  });
  expect(getQuery({endpoint: 'getuserstarredrepos'})).toEqual({});
  expect(getQuery({endpoint: 'getuserstarredrepos', login: 'renatogalvones'})).toEqual({
    query: getUserStarredRepos,
    variables: {
      first: 0,
      login: 'renatogalvones',
    }
  });
  expect(getQuery({endpoint: 'addstar'})).toEqual({});
  expect(getQuery({endpoint: 'addstar', starrableId: '9999999999999'})).toEqual({
    query: addStar,
    variables: {
      input: {
        starrableId: '9999999999999',
      }
    }
  });
  expect(getQuery({endpoint: 'removestar'})).toEqual({});
  expect(getQuery({endpoint: 'removestar', starrableId: '9999999999999'})).toEqual({
    query: removeStar,
    variables: {
      input: {
        starrableId: '9999999999999',
      }
    }
  });
});