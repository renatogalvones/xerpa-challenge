const getLoggedUserInfo = require('../../graphql/queries/getLoggedUserInfo');
const getUserStarredRepos = require('../../graphql/queries/getUserStarredRepos');
const addStar = require('../../graphql/queries/addStar');
const removeStar = require('../../graphql/queries/removeStar');

const getQuery = ({ endpoint, login, starrableId }) => {

  switch (endpoint) {
    case 'getloggeduserinfo':
      return {
        query: getLoggedUserInfo,
      }

    case 'getuserstarredrepos':
      if (!login) return {};
      return {
        query: getUserStarredRepos,
        variables: {
          login: login,
          first: 0,
        }
      }

    case 'addstar':
      if (!starrableId) return {};
      return {
        query: addStar,
        variables: {
          input: {
            starrableId: starrableId
          }
        }
      }

    case 'removestar':
      if (!starrableId) return {};
      return {
        query: removeStar,
        variables: {
          input: {
            starrableId: starrableId
          }
        }
      }

    default:
      return {};
  }
}

module.exports = getQuery;
