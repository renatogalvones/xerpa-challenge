require('dotenv').config();
var cors = require('cors');
const express = require('express');
const axios = require('axios');

const app = express();

app.use(cors({ origin: 'http://localhost:3000' }));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const getQuery = require('./helpers/index');

app.get('/gettoken', (req, res) => {
  const { code } = req.query;
  if (!code) res.status(400).json({ message: 'No code has been passed' });

  const reqBody = {
    client_id: process.env.REACT_APP_CLIENT_ID,
    client_secret: process.env.REACT_APP_CLIENT_SECRET,
    code,
    redirect_uri: process.env.REACT_APP_REDIRECT_URI,
  };

  axios
    .post(`https://github.com/login/oauth/access_token`, reqBody)
    .then((response) => {
      res.status(200).json(response.data);
    })
    .catch(error => res.status(400).json({ message: 'Invalid code' }));

});

app.post('/github-graphql', (req, res) => {
  const { authorization } = req.headers;
  const { endpoint, login, starrableId } = req.body;

  const headers = {
    'Authorization': authorization,
    'Content-Type': 'application/json',
  };

  const query = getQuery({ endpoint, login, starrableId});

  axios
    .post('https://api.github.com/graphql', JSON.stringify(query), { headers: headers })
    .then((result) => {
      res.status(200).json(result.data);
    })
    .catch((error) => { res.status(400).send(error); console.log('error', error); })
});

module.exports = app;