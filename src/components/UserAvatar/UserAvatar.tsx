import { Wrapper } from './style';

interface UserAvatarProps {
  avatarUrl: string,
}

const IconText = ({ avatarUrl }:UserAvatarProps ) => {
  return (
    <Wrapper>
      <img src={avatarUrl} alt="" />
    </Wrapper>
  )
}

export default IconText;
