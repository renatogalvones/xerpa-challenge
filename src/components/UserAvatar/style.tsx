import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: left;
  padding: 0 7px;
  margin-bottom: 13px;
  display: flex;
  align-items: center;

  img {
    clip-path: circle(32px at center);
    max-width: 64px;
  }
`;
