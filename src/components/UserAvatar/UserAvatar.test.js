import { render, screen } from '@testing-library/react';

import UserAvatar from './UserAvatar';

test('UserAvatar component', () => {
  const { container } = render(<UserAvatar avatarUrl="https://avatars.githubusercontent.com/u/1132274?v=4" />);
  expect(container).toMatchSnapshot();
})