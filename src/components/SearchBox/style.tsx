import styled from 'styled-components';

import { primary } from '../../theme/pallet';

interface Layout {
  layout: 'default' | 'basic';
}


export const Wrapper = styled.div <Layout>`
  width: 100%;
  height: 91px;
  border-radius: 5px;
  display: flex;
  align-items: center;
  padding: 37px;
  position: relative;
  ${(props: Layout) => props.layout === 'basic' ? '' : `box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.15);`}


  form {
    width: 100%;
  }
`;

export const Search = styled.input <Layout>`
  font-weight: 400;
  color: ${primary.alpha100};
  font-size: 16px;
  line-height: 19px;
  width: 100%;
  border: none;
  outline: none;
  background-color: transparent !important;
  ${(props: Layout) => props.layout === 'basic' ? `border-bottom: solid 1px ${primary.alpha300};` : ''}
`;

export const SearchIcon = styled.img <Layout>`
  width: 17px;
  height: 16px;
  position: absolute;
  right: 0;
  margin-right: 40px;
  ${(props: Layout) => props.layout === 'basic' ? `top: calc(50% - 13px);` : 'top: calc(50% - 9px);'}
`;
