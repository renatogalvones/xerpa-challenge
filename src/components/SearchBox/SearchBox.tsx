import searchIcon from '../../assets/icons/search.svg';

import { Wrapper, Search, SearchIcon } from './style';

export interface SearchAttr {
  layout: 'default' | 'basic';
  placeholder?: string;
  handleSubmit: (e: any) => void;
}

const SearchBox = ({ layout, handleSubmit, placeholder }: SearchAttr) => {
  const submit = (e: any) => {
    e.preventDefault();

    handleSubmit(e.target.elements.searchTerm.value);
  }

  return (
    <Wrapper layout={layout}>
      <form onSubmit={submit} role="search">
        <Search layout={layout} name="searchTerm" placeholder={placeholder} />
        <SearchIcon layout={layout} src={searchIcon} />
      </form>
    </Wrapper>
  )
}

export default SearchBox;
