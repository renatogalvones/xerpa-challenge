import { fireEvent, render, screen } from '@testing-library/react';

import SearchBox from './SearchBox';


test('SearchBox component default', () => {
  const handleSubmitMock = jest.fn();
  const { container } = render(<SearchBox layout="default" placeholder="nice placeholder" handleSubmit={handleSubmitMock} />);
  expect(screen.getByPlaceholderText('nice placeholder')).toBeTruthy();
  fireEvent.submit(screen.getByRole('search'));
  expect(handleSubmitMock).toHaveBeenCalledTimes(1);
  expect(container).toMatchSnapshot();
})

test('SearchBox component basic', () => {
  const handleSubmitMock = jest.fn();
  const { container } = render(<SearchBox layout="default" placeholder="nice placeholder" handleSubmit={handleSubmitMock} />);
  expect(screen.getByPlaceholderText('nice placeholder')).toBeTruthy();
  fireEvent.submit(screen.getByRole('search'));
  expect(handleSubmitMock).toHaveBeenCalledTimes(1);
  expect(container).toMatchSnapshot();
})
