import { render, screen } from '@testing-library/react';

import Card from './Card';

test('Card component', () => {
  const { container } = render(<Card repoName="reponame/reponame" repoDescription="A nice repo description" starsNumber="30" starred handleClick={() => {}} />)
  expect(screen.getByText('reponame/reponame')).toBeTruthy();
  expect(screen.getByText('A nice repo description')).toBeTruthy();
  expect(container).toMatchSnapshot();
})