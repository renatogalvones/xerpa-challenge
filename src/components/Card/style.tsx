import styled from 'styled-components';

import { primary } from '../../theme/pallet';

export const Wrapper = styled.article`
  background: ${primary.alpha500};
  box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.15);
  border-radius: 5px;
  padding: 27px 32px 32px;
  margin-bottom: 33px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const RepoName = styled.h3`
  margin: 0;
  font-weight: 400;
  display: block;
  font-size: 20px;
  line-height: 24px;
  margin-bottom: 7px;
`;

export const RepoDescription = styled.span`
  display: block;
  font-size: 15px;
  font-weight: 300;
  line-height: 19px;
  margin-bottom: 7px;
`;

export const Stars = styled.div`
  display: flex;
  align-items: center;
  font-weight: 300;
`;

export const Icon = styled.img`
  padding-right: 7px;
`;

interface ButtonProps {
  starred: boolean;
}

export const Button = styled.button<ButtonProps>`
  background-color: ${primary.alpha500};
  border: 2px solid ${primary.alpha600};
  border-radius: 5px;
  color: ${primary.alpha600};
  font-size: 16px;
  padding: 12px 0;
  width: 6.3%;
  min-width: 91px;
  text-align: center;
  cursor: pointer;
  ${(props) => props.starred ? `background-color: ${primary.alpha600}; color: ${primary.alpha500};` : ''}
`;

