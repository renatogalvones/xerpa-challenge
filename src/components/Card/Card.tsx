import starIcon from '../../assets/icons/star.svg';

import {
  Wrapper,
  RepoName,
  RepoDescription,
  Stars,
  Button,
  Icon,
} from './style';

interface CardProps {
  repoName: string,
  repoDescription: string,
  starsNumber: number,
  starred: boolean,
  handleClick: (e: any) => void,
}


const Card = ({
  repoName,
  repoDescription,
  starsNumber,
  starred,
  handleClick,
}: CardProps) => {
  return (
    <Wrapper>
      <div>
        <RepoName>{repoName}</RepoName>
        <RepoDescription>{repoDescription}</RepoDescription>
        <Stars><Icon src={starIcon} />{starsNumber}</Stars>
      </div>
      <Button starred={starred} onClick={handleClick}>{starred ? 'unstar' : 'star'}</Button>
    </Wrapper>
  )
}

export default Card;
