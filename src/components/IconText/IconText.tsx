import userIcon from '../../assets/icons/user.svg';
import locationIcon from '../../assets/icons/location.svg';
import globeIcon from '../../assets/icons/globe.svg';
import mailIcon from '../../assets/icons/mail.svg';

import { IconWrapper, Icon, Text } from './style';

interface IconTextProps {
  icon: 'user' | 'location' | 'globe' | 'mail',
  text: string,
}

const IconText = ({ icon, text }:IconTextProps ) => {
  const icons = {
    user: userIcon,
    location: locationIcon,
    globe: globeIcon,
    mail: mailIcon,
  }

  return (
    <IconWrapper>
      <Icon src={icons[icon]} />
      <Text>{text}</Text>
    </IconWrapper>
  )
}

export default IconText;
