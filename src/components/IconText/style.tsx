import styled from 'styled-components';

import { primary } from '../../theme/pallet';

export const IconWrapper = styled.div`
  text-align: left;
  padding: 0 7px;
  margin-bottom: 13px;
  display: flex;
  align-items: center;
`;

export const Icon = styled.img`
  padding-right: 10px;
`;

export const Text = styled.span`
  font-size: 14px;
  color: ${primary.alpha500};
`;

