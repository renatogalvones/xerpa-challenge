import { render, screen } from '@testing-library/react';

import IconText from './IconText';

test('IconText component globe', () => {
  const { container } = render(<IconText icon="globe" text="test globe" />);
  expect(screen.getByText('test globe')).toBeTruthy();
  expect(container).toMatchSnapshot();
})

test('IconText component location', () => {
  const { container } = render(<IconText icon="location" text="test location" />);
  expect(screen.getByText('test location')).toBeTruthy();
  expect(container).toMatchSnapshot();
})

test('IconText component mail', () => {
  const { container } = render(<IconText icon="mail" text="test mail" />);
  expect(screen.getByText('test mail')).toBeTruthy();
  expect(container).toMatchSnapshot();
})

test('IconText component user', () => {
  const { container } = render(<IconText icon="user" text="test user" />);
  expect(screen.getByText('test user')).toBeTruthy();
  expect(container).toMatchSnapshot();
})