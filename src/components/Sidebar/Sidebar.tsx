import IconText from '../IconText';

import {
  Wrapper,
  DarkerBackground,
  Figure,
  FullName,
  Username,
  UserParagraph,
  Details,
} from './style';

interface SidebarAttr {
  fullname: string;
  username: string;
  avatarUrl: string;
  bio: string;
  followers: string;
  location: string;
  email: string;
  websiteUrl: string;
}

const Sidebar = ({
  fullname,
  username,
  avatarUrl,
  bio,
  followers,
  location,
  email,
  websiteUrl,
}: SidebarAttr) => {
  return (
    <Wrapper>
      <DarkerBackground>
        <Figure src={avatarUrl} alt="" />
        <FullName>{fullname}</FullName>
        <Username>{username}</Username>
      </DarkerBackground>
      <Details>
        <UserParagraph>{bio}</UserParagraph>
        {followers && <IconText icon="user" text={`${followers}k`} />}
        {location && <IconText icon="location" text={location} />}
        {email && <IconText icon="mail" text={email} />}
        {websiteUrl && <IconText icon="globe" text={websiteUrl} />}
      </Details>


    </Wrapper>
)
}

export default Sidebar;
