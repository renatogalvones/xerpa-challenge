import styled from 'styled-components';

import { primary } from '../../theme/pallet';

export const Wrapper = styled.aside`
  background-color: ${primary.alpha200};
  box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
  border-radius: 5px;
  color: ${primary.alpha500};
  max-width: 230px;
  text-align: center;
  margin-top: 32px;
`

export const DarkerBackground = styled.div`
  background: #00000017;
  padding: 30px 15px;
`;

export const Figure = styled.img`
  clip-path: circle(80px at center);
  max-width: 160px;
  margin-bottom: 17px;
`;

export const FullName = styled.h2`
  margin: 0;
  font-size: 20px;
  font-weight: 400;
  line-height: 24px;
`;

export const Username = styled.h3`
  margin: 0;
  font-size: 20px;
  font-weight: 300;
  line-height: 24px;
`;

export const Details = styled.div`
  padding: 16px;
`;

export const UserParagraph = styled.p`
  font-size: 14px;
  font-weigth: 400;
  line-height: 17px;
  text-align: left;
  margin-bottom: 25px;
`;