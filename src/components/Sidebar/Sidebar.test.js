import { render, screen } from '@testing-library/react';

import Sidebar from './Sidebar';

test('Sidebar component', () => {
  const { container } = render(
    <Sidebar
      fullname="Rafa Siqueira"
      username="rafaelsiqueira"
      avatarUrl="https://avatars.githubusercontent.com/u/1132274?v=4"
      followers="55"
      location="São Paulo, SP"
      email="siqueira@tesla.com"
      websiteUrl="www.rafaenterprises.com"
    />
  );
  expect(screen.getByText("Rafa Siqueira")).toBeTruthy();
  expect(screen.getByText("rafaelsiqueira")).toBeTruthy();
  expect(screen.getByText("São Paulo, SP")).toBeTruthy();
  expect(screen.getByText("siqueira@tesla.com")).toBeTruthy();
  expect(screen.getByText("www.rafaenterprises.com")).toBeTruthy();
  expect(container).toMatchSnapshot();
})