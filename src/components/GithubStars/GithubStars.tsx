import { Thin, Regular, Wrapper } from './style';

const GithubStars = () => {
  return (<Wrapper><Thin>Github</Thin><Regular>Stars</Regular></Wrapper>)
}

export default GithubStars;
