import styled from 'styled-components';

import { primary } from '../../theme/pallet';

const Thin = styled.span`
  font-weight: 100;
  color: ${primary.alpha100};
`;

const Regular = styled.span`
  font-weight: 400;
  color: ${primary.alpha200};
`;

const Wrapper = styled.h1`
  margin: 0;
`;


export { Thin, Regular, Wrapper };
