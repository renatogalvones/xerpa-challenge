import { render, screen } from '@testing-library/react';

import GithubStars from './GithubStars';

test('GithubStars component', () => {
  const { container } = render(<GithubStars />);
  expect(container).toMatchSnapshot();
})