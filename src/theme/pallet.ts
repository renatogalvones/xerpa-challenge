export const primary = {
  alpha100: '#4F4F4F',
  alpha200: '#5253B9',
  alpha300: '#BABABA',
  alpha400: '#00000017',
  alpha500: '#FFFFFF',
  alpha600: '#5152B7',
};