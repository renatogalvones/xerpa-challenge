export const userInitialState = {
  avatarUrl: '',
  bio: '',
  location: '',
  login: '',
  name: '',
};

export const userReducer = (state, action) => {
  switch (action.type) {
    case "SET_USER": {
      return {
        ...state,
        avatarUrl: action.data.avatarUrl,
        bio: action.data.bio,
        location: action.data.location,
        login: action.data.login,
        name: action.data.name,
      }
    }
    default:
      return state;
  }
};
