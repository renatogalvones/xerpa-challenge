export const initialState = {
  accessToken: localStorage.getItem('accessToken'),
  clientId: process.env.REACT_APP_CLIENT_ID,
  redirectUri: process.env.REACT_APP_REDIRECT_URI,
  clientSecret: process.env.REACT_APP_CLIENT_SECRET,
  proxyUrl: process.env.REACT_APP_PROXY_URL
};

export const reducer = (state, action) => {
  switch (action.type) {
    case "SAVE_TOKEN": {
      localStorage.setItem('accessToken', action.data.accessToken);
      return {
        ...state,
        accessToken: action.data.accessToken,
      }
    }
    default:
      return state;
  }
};
