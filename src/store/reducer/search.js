export const searchInitialState = {
  avatarUrl: '',
  bio: '',
  location: '',
  login: '',
  name: '',
  email: '',
  starredRepositories: {},
  followers: {},
  websiteUrl: '',
};

export const searchReducer = (state, action) => {
  switch (action.type) {
    case "SET_DATA": {
      return {
        ...state,
        ...action.data,
        avatarUrl: action.data.avatarUrl,
        bio: action.data.bio,
        location: action.data.location,
        login: action.data.login,
        name: action.data.name,
      }
    }
    case "UPDATE_STAR": {
      const { id, active } = action.data;

      const starred = state.starredRepositories.edges.map((repo) => {
        if (id === repo.node.id) {
          return {
            node: {
              ...repo.node,
              stargazerCount: repo.node.stargazerCount + (active ? -1 : 1 ),
              viewerHasStarred: !active,
            }
          }
        }
        return repo;
      });

      return {
        ...state,
        starredRepositories: {
          edges: starred
        },
      }
    }
    default:
      return state;
  }
};
