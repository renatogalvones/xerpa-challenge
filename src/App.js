import React, { createContext, useReducer } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { initialState, reducer } from "./store/reducer";
import { userInitialState, userReducer } from './store/reducer/user';
import { searchInitialState, searchReducer } from './store/reducer/search';

import Login from './pages/Login';
import Search from './pages/Search';
import Result from './pages/Result';
import NotFound from './pages/NotFound';

import { Wrapper } from './style.tsx';


export const AuthContext = createContext();
export const UserContext = createContext();
export const SearchContext = createContext();

const App = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [userState, userDispatch] = useReducer(userReducer, userInitialState);
  const [searchState, searchDispatch] = useReducer(searchReducer, searchInitialState);

  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      <UserContext.Provider value={{ userState, userDispatch }}>
        <SearchContext.Provider value={{ searchState, searchDispatch }}>
          <Wrapper>
            <Router>
              <Switch>
                <Route path="/login" exact component={Login} />
                <Route path="/result" component={Result} />
                <Route path="/notfound" component={NotFound} />
                <Route path="/" component={Search} />
              </Switch>
            </Router>
          </Wrapper>
        </SearchContext.Provider>
      </UserContext.Provider>
    </AuthContext.Provider>

  );
}

export default App;
