# Xerpa Challenge - GitHub Stars
A SPA (Single Page Application) for starring repositories while looking on other users starred repos.

## Introduction
I've decided to work with a proxy for the OAuth step to fulfill the challenge requirements. On other ocasions I've used react-apollo to communicate with GraphQL endpoints. Also I found interesting to add some typed functions as I'm styding it. The choice for styled-components it's because I find it very useful, stylish and organized to maintain components. And afterall, I've tried to maintain the code as simple as possible, easy to read and clean to not upset you guys when looking at it.

## Challenges
This was the first time I used backend to work with GraphQL so this was a bit challenging but it was cool to understand how it works.

## How to run it
You need to keep 2 processes in order for the application to run. Follow the steps below:


```
# Clone the repository
$ git clone https://gitlab.com/renatogalvones/xerpa-challenge

# Select the right version of node
$ nvm use

# Install dependencies
$ yarn

# run the proxy
$ yarn server

# run the app
$ yarn start
```

## Running tests
For the sake of this test I've written some tests for the components and to cover the unique token free endpoint on the proxy.
Imagining the evolution of the app, would be nice to implement some cypress tests to cover the user experience (hitting some mocked endpoints maybe) but I've not added this time to save some time.

```
# To run
$ yarn test

```

## Conclusion

Thanks for the opportunity. It was really cool developing this proxy and taking care of OAuth along with backend. Hope you guys enjoy it!
